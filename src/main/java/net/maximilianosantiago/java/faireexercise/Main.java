package net.maximilianosantiago.java.faireexercise;

public class Main {

    public static void main(String[] args) {
        RetrofitHandler handler = new RetrofitHandler();
        handler.start();
    }
}
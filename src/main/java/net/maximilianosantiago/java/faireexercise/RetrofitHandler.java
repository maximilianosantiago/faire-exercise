package net.maximilianosantiago.java.faireexercise;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHandler implements Callback<List<Product>> {

    static final String BASE_URL = "https://www.faire.com/api/v1/";

    public void start() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        FaireAPI faireAPI = retrofit.create(FaireAPI.class);

        Call<List<Product>> call = faireAPI.getAllProducts("b_d2481b88");
        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
        if(response.isSuccessful()) {
            List<Product> productsList = response.body();
            productsList.forEach(product -> System.out.println("id: " + product.id + " == " + product.name));
            //THE ACTION STARTS HERE!
        } else {
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<List<Product>> call, Throwable t) {
        t.printStackTrace();
    }
}
package net.maximilianosantiago.java.faireexercise;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface FaireAPI {

    @Headers("X-FAIRE-ACCESS-TOKEN : HQLA9307HSLQYTC24PO2G0LITTIOHS2MJC8120PVZ83HJK4KACRZJL91QB7K01NWS2TUCFXGCHQ8HVED8WNZG0KS6XRNBFRNGY71")
    @GET("products/")
    Call<List<Product>> getAllProducts(@Query("brand_id") String brandId);

    @Headers("X-FAIRE-ACCESS-TOKEN : HQLA9307HSLQYTC24PO2G0LITTIOHS2MJC8120PVZ83HJK4KACRZJL91QB7K01NWS2TUCFXGCHQ8HVED8WNZG0KS6XRNBFRNGY71")
    @GET("orders/")
    Call<List<Order>> getAllOrders();
}
